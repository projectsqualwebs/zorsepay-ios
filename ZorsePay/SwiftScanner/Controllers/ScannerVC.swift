//
//  ScannerVC.swift
//  SwiftScanner
//
//  Created by Jason on 2018/11/30.
//  Copyright © 2018 Jason. All rights reserved.
//

import UIKit
import AVFoundation
import SDWebImage

public class ScannerVC: UIViewController {
    //MARK: IBoutlets
    @IBOutlet weak var bottomView: View!
    @IBOutlet weak var topView: UIView!
//    @IBOutlet weak var codeView: View!
//    @IBOutlet weak var topupButton: CustomButton!
//    @IBOutlet weak var blackHeading: UILabel!
//    @IBOutlet weak var yellowHeading: UILabel!
//    @IBOutlet weak var qrField: DesignableUITextField!
    @IBOutlet weak var searchField: DesignableUITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var contactCollection: UICollectionView!
    
    
    var isScanPay = false
    
    public lazy var headerViewController:HeaderVC = .init()
    
    public lazy var cameraViewController:CameraVC = .init()
    
    /// 动画样式
    public var animationStyle:ScanAnimationStyle = .default{
        didSet{
            cameraViewController.animationStyle = animationStyle
        }
    }
    
    // 扫描框颜色
    public var scannerColor:UIColor = .white{
        didSet{
            cameraViewController.scannerColor = scannerColor
        }
    }
    
    public var scannerTips:String = ""{
        didSet{
            cameraViewController.scanView.tips = scannerTips
        }
    }
    
    /// `AVCaptureMetadataOutput` metadata object types.
    public var metadata = AVMetadataObject.ObjectType.metadata {
        didSet{
            cameraViewController.metadata = metadata
        }
    }
    
    public var successBlock:((String)->())?
    
    public var errorBlock:((Error)->())?
    
    
    /// 设置标题
    public override var title: String?{
        
        didSet{
            
            if navigationController == nil {
                headerViewController.title = title
            }
        }
        
    }
    
    
    /// 设置Present模式时关闭按钮的图片
    public var closeImage: UIImage?{
        
        didSet{
            
            if navigationController == nil {
                headerViewController.closeImage = closeImage ?? UIImage()
            }
        }
        
    }
    
    var contactData = [TransactionResponse]()
    var searchData = [TransactionResponse]()
    var searchText = ""
    
    override public func viewDidLoad() {
        super.viewDidLoad()
//        self.qrField.delegate = self
//        if(isScanPay){
//            self.bottomView.isHidden = true
//            self.yellowHeading.text = "PAY"
//            self.blackHeading.text = "SCAN N "
//        }
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getContactData { (data) in
            self.contactData = data
            self.contactCollection.reloadData()
        }
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        cameraViewController.startCapturing()
        self.view.bringSubviewToFront(cameraViewController.view)
        self.view.bringSubviewToFront(topView)
       self.view.bringSubviewToFront(bottomView)
    }
    
    //MARK: IBAction
    @IBAction func scanAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanDetailViewController") as! ScanDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
      //  self.navigationController?.popViewController(animated: true)
        Singleton.shared.showToast(text: "Successfully scanned the document.")
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchBarViewController") as! SearchBarViewController
//        self.navigationController?.pushViewController(myVC, animated: true)
        if(searchIcon.image == #imageLiteral(resourceName: "contacts")){
          searchIcon.image = #imageLiteral(resourceName: "clear")
            self.searchField.becomeFirstResponder()
            self.searchData = []
            self.searchField.text = ""
            self.searchText = ""
            self.contactCollection.reloadData()
            self.searchField.isHidden = false
        }else {
          searchIcon.image = #imageLiteral(resourceName: "contacts")
            self.searchField.text = ""
            self.searchText = ""
            self.searchData = []
            self.contactCollection.reloadData()
            self.searchField.resignFirstResponder()
            self.searchField.isHidden = true
        }
    }
}

// MARK: - CustomMethod
extension ScannerVC{
    
    func setupUI() {
        
        if title == nil {
            title = "Zorse Pay Scan"
        }
        
        view.backgroundColor = K_PRIMARY_COLOR.withAlphaComponent(0.4)
        
        headerViewController.delegate = self
        
        cameraViewController.metadata = metadata
        
        cameraViewController.animationStyle = animationStyle
        
        cameraViewController.delegate = self
        
        add(cameraViewController)
        
        if navigationController == nil {
           // add(headerViewController)
           // view.bringSubviewToFront(headerViewController.view)
        }
        
        
    }
    
    
    public func setupScanner(_ title:String? = nil, _ color:UIColor? = nil, _ style:ScanAnimationStyle? = nil, _ tips:String? = nil, _ success:@escaping ((String)->())){
        
        if title != nil {
            self.title = title
        }
        
        if color != nil {
            scannerColor = color!
        }
        
        if style != nil {
            animationStyle = style!
        }
        
        if tips != nil {
            scannerTips = tips!
        }
        
        successBlock = success
        
    }
}

// MARK: - HeaderViewControllerDelegate
extension ScannerVC:HeaderViewControllerDelegate{
    
    
    /// 点击关闭
    public func didClickedCloseButton() {
        dismiss(animated: true, completion: nil)
        
    }
    
}


extension ScannerVC:CameraViewControllerDelegate{
    func didOutput(_ code: String) {
        successBlock?(code)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as! AddMoneyViewController
        myVC.actionType = 3
        myVC.emailID = code
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func didReceiveError(_ error: Error) {
        errorBlock?(error)
    }
}

extension ScannerVC: UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.contactData.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionCell", for: indexPath) as! CardCollectionCell
        let val = self.contactData[indexPath.row]
        if(val.receiver_id == Singleton.shared.userDetail.id){
            cell.cardImage.sd_setImage(with: URL(string:self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.cardholderName.text = val.sender_name
        }else {
            cell.cardImage.sd_setImage(with: URL(string:self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.cardholderName.text = val.receiver_name ?? "You"
        }
        return cell
    }
}
