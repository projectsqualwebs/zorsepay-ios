
import UIKit


//COLORS

let K_PRIMARY_COLOR = UIColor(red: 28/255, green: 35/255, blue: 132/255, alpha: 1)//0C0C0C
let K_GREEN_COLOR = UIColor(red: 0/255, green: 129/255, blue: 13/255, alpha: 1)//00800D
let K_RED_COLOR = UIColor(red: 139/255, green: 0/255, blue: 0/255, alpha:1)//8b0000
var K_WHITE_COLOR = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)//#ffffff
var K_YELLOW_COLOR = UIColor(red: 255/255, green: 155/255, blue: 5/255, alpha: 1)//#FF9B05
var K_BLACK_COLOR = UIColor(red: 12/255, green: 12/255, blue: 12/255, alpha: 1)//#0C0C0C
var K_LIGHT_GRAY = UIColor(red: 159/255, green: 159/255, blue: 159/255, alpha: 1)//9F9F9F
var K_OFF_WHITE_COLOR = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)//#e5e5e5

//APIs
let U_BASE = "http://54.176.179.158/zorsepay/api/"
let U_IMAGE_BASE = "http://54.176.179.158/zorsepay/storage/app/public/"
let U_QR_BASE_URL = "http://54.176.179.158/zorsepay"

let U_LOGIN = "login"
let U_REGISTER = "register"
let U_VERIFY_OTP = "verify-otp"
let U_SEND_OTP = "send-otp"
let U_FORGOT_PASS = "forget-password"
let U_VERIRFY_FORGET_PASS_OTP = "verify-otp-forget-password"
let U_RESET_PASSWORD = "reset-password"
let U_CHANGE_PASSWORD = "user/change-password"
let U_CHANGE_PIN = "user/update-security-pin"
let U_DOCUMENT_UPLOAD = "document-upload"
let U_ADD_SUPPORT_QUERY = "add-support-query"
let U_GET_SUPPORT_QUERY = "get-support-query"

let U_GET_DASHBOARD_DATA = "user/dashboard"

let U_GET_CONTACTS = "user/contacts"
let U_SEARCH_CONTACTS = "user/search"
let U_RETREIVE_BANK_ACCOUNT = "user/retrive-bank-account"
let U_UPDATE_BANK_ACCOUNT = "user/update-bank-account"
let U_SENDTO_BANK_ACCOUNT = "user/send-to-bank-account"
let U_REGISTER_BANK_ACCOUNT = "user/register-bank-account"

let U_GET_PROFILE = "user/me"
let U_UPDATE_PROFILE = "user/update-profile"
let U_UPLOAD_PROFILE_IMAGE = "user/profile-image-upload"
let U_UPLOAD_DOCUMENT_IMAGE = "user/document-upload"
let U_UPLOAD_IMAGE = "image-upload"

let U_GET_WALLETS = "wallet/all"
let U_ADD_MONEY_WALLET = "wallet/add-money"
let U_GET_CARDS = "user/get-saved-methods"
let U_SEARCH_USER = "user/search"
let U_WITHDRAW_MOENY = "wallet/withdraw-money"
let U_REQUEST_MONEY = "wallet/request-money"
let U_REFER_FRIEND = "refer-friend"

let U_GET_PAYMENT_METHOD = "payment/get-payment-methods"
let U_ADD_CARD = "payment/add-card"
let U_GET_ALL_TRANSACTIONS = "payment/transactions"
let U_GET_ALL_CURRENCIES = "payment/get-currencies"
let U_GET_CONVERT_CURRENCY = "payment/get-base-exchange-amount"
let U_GET_SINGLE_TRANSACTION = "payment/transaction/1"
let U_DELETE_CARD = "payment/delete-card"
let U_CALCULATE_COMISSION = "calculate-commission"

let U_GET_COUNTRIES = "get-countries"
let U_GET_CITIES = "get-cities/"
let U_GET_POSTCODE_CITY = "get-postcodes/"


let U_GENERATE_WALLET_TRANSFER_OTP = "wallet/generate-otp-for-transfer"
let U_WALLET_TRANSFER = "wallet/transfer"
let U_COD_TRANSFER = "wallet/cod-transfer"


let U_GET_POSTCODE = "postcodes"
let U_GET_SERVICE_STATION = "service-stations/"


//Constants


//Notification Center
let N_BOTTOM_NAVIGATION_OPTION = "change_bottom_navigation_name"
let N_STOP_BUTTON_ANIMATION = "N_STOP_BUTTON_ANIMATION"

//User Default's
var UD_FCM_TOKEN = "fcm_token"
var UD_INITIAL_VIEW_CONTROLLER = "initial_view_controller"
var UD_USER_EMAIL = "user_email_address"
var UD_USER_DETAIL = "user_detail"
var UD_TOKEN = "access_token"
var UD_IS_FIRST_TIME = "launch_first_time"
var UD_CURRENT_BOTTOM_OPTION = "current_selected_option"
