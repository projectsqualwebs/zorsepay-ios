//
//  PickerViewController.swift
//  ITreat
//
//  Created by qw on 10/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

protocol SelectFromPicker {
    func selectedItem(name:String,id: Int)
}

class PickerViewController: UIViewController,
UIPickerViewDelegate, UIPickerViewDataSource{
    //MARK: IBOutlets
     @IBOutlet weak var pickerView: UIPickerView!
     
     var pickerDelegate:SelectFromPicker? = nil
     var pickerData = [String]()
     
     var index = Int()
     
     override func viewDidLoad() {
         super.viewDidLoad()
         self.pickerView.delegate = self
     }


    //MARK: IBActions
     @IBAction func doneAction(_ sender: Any) {
        self.pickerDelegate?.selectedItem(name: self.pickerData[index ],id:(index ?? 0))
         self.dismiss(animated: true, completion: nil)
     }
     
     func numberOfComponents(in pickerView: UIPickerView) -> Int {
      return 1
     }
        
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      return self.pickerData.count
     }
     
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return self.pickerData[row]
     }
     
     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         self.index = row
     }
 }
