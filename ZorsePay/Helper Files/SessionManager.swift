//
//  APIManager.swift
//
//  Created by Qualwebs on 02/01/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//
import UIKit
import Alamofire


class SessionManager: NSObject {
    
    static var shared = SessionManager()
    
    var createWallet: Bool = true
    
    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
        print("TOKEN: \(getHeader(reqCode: requestCode))")
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers:  getHeader(reqCode: requestCode), interceptor: nil).responseString { (dataResponse) in
            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")
            
            switch dataResponse.result {
            case .success(_):
                var object:T?
                if(statusCode != 400){
                    object = self.convertDataToObject(response: dataResponse.data, T.self)
                }else if(statusCode == 200){
                    object = self.convertDataToObject(response: dataResponse.data, T.self)
                }
                let errorObject = self.convertDataToObject(response: dataResponse.data, ErrorResponse.self)
                if (statusCode == 200 || statusCode == 201) && object != nil{
                    completionHandler(object!)
                } else if statusCode == 404  {
                    Singleton.shared.showToast(text: errorObject?.message ?? "")
                   // NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                }else if statusCode == 400{
                    
                    if(errorObject?.message != "" || errorObject?.message != nil){
                        Singleton.shared.showToast(text: errorObject?.message ?? "")
                    }
                  //  NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                    
                } else {
                    Singleton.shared.showToast(text: errorObject?.message ?? "")
                   // NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                    
                    
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
                // ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    if(error != "" || error != nil){
                    }
                    
                 //   NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": error ?? ""])
                } else {
                    
                  //  NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": error ?? ""])
                    
                }
                break
            }
        }
    }
    
    
    func makeMultipartRequest(url: String, fileData: Data?, param: [String:Any], fileName: String, completionHandler: @escaping (Any) -> Void) {
        AF.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in param {
                if let temp = value as? String {
                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                        if let num = element as? Int {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            
            if let data = fileData{
                multipartFormData.append(data, withName: "file", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
            }
        },
        to: url, method: .post , headers: getHeader(reqCode: U_UPLOAD_IMAGE))
        .responseJSON(completionHandler: { (response) in
            
            print(response)
            
            if let err = response.error{
                print(err)
                // onError?(err)
                return
            }
            print("Succesfully uploaded")
            
            let json = response.data
            
            if (json != nil)
            {
                let jsonObject = self.convertDataToObject(response: response.data, UploadImageResponse.self)
                completionHandler(jsonObject)
            }
        })
    }
    
    private func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    
    func getHeader(reqCode: String) -> HTTPHeaders? {
        let token = UserDefaults.standard.string(forKey: UD_TOKEN)
        if (reqCode != U_LOGIN) && (reqCode != U_REGISTER) && (reqCode != U_RESET_PASSWORD) && (reqCode != U_VERIFY_OTP) && (reqCode != U_VERIRFY_FORGET_PASS_OTP) && (reqCode != U_FORGOT_PASS) {
            if(token == nil){
                return nil
            }else {
                return ["Authorization": "Bearer " + token!]
            }
        } else {
            return nil
        }
    }
}
