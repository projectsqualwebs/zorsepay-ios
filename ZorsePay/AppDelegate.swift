//
//  AppDelegate.swift
//
//  Created by qw on 14/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import GooglePlaces
//import GoogleMapsBase

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Handling smart keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
      //  initializeGoogleMap()
        return true
    }
    
    func initializeGoogleMap(){
        // GMSServices.provideAPIKey("AIzaSyAZTQHLuxlQSaiYnb3l_nkQX96bHbhbYEo")
      //  GMSPlacesClient.provideAPIKey("AIzaSyAZTQHLuxlQSaiYnb3l_nkQX96bHbhbYEo")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if(dontRememberMe){
            UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        }
    }
}

