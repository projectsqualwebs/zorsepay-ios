//
//  SplashViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController, PageContentIndexDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var registerButton: CustomButton!
    
    
    var currentIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SinglePageViewController.indexDelegate = self
        initView()
    }

    func initView(){
        self.registerButton.setTitle("Sign Up", for: .normal)
    }
    
    func getContentIndex(index: Int) {
     
    }
    //MARK: IBActions
    @IBAction func changePage(_ sender: Any) {
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        myVC.backButtonVisible = false
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
    @IBAction func registerAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}

