//
//  SelectCardViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

protocol SelectedCard {
    func selectedCardDetail(type:Int, card: GetCardResponse)
}

class SelectCardViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var bankTable: ContentSizedTableView!
    @IBOutlet weak var addCardButton: CustomButton!
    @IBOutlet weak var addMoneyButton: CustomButton!
    @IBOutlet weak var addCardView: UIStackView!
    @IBOutlet weak var bankCardView: UIStackView!
    
    @IBOutlet weak var cardName: DesignableUITextField!
    @IBOutlet weak var cardNumber: DesignableUITextField!
    @IBOutlet weak var cvvField: DesignableUITextField!
    @IBOutlet weak var monthField: DesignableUITextField!
    @IBOutlet weak var yearField: DesignableUITextField!
    @IBOutlet weak var walletAmount: DesignableUITextField!
    
    
    var cardDelegate:SelectedCard?  = nil
    var walletData = [WalletResponse]()
    var cardData = [GetCardResponse]()
    var selectedCard = GetCardResponse()
    var isWalletSelected  = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cardData = Singleton.shared.cardData
        self.walletData = Singleton.shared.walletData
        initView()
    }
    
    func initView(){
        self.cardNumber.delegate = self
        self.monthField.delegate = self
        self.yearField.delegate = self
        self.cvvField.delegate = self
        self.cardNumber.text = ""
        self.monthField.text = ""
        self.yearField.text = ""
        self.cvvField.text = ""
        self.walletAmount.text = ""
        self.bankCardView.isHidden = true
        self.addCardView.isHidden = true
        self.addMoneyButton.backgroundColor = .lightGray
        self.addCardButton.backgroundColor = .lightGray
        self.addMoneyButton.setTitle("+ Money to Wallet", for: .normal)
        self.addCardButton.setTitle("+ Card", for: .normal)
    }
    
    func callAPI(){
        NavigationController.shared.getWalletData(completionHandler: { (data) in
            self.walletData = data
            self.bankTable.reloadData()
        })
        
        NavigationController.shared.getCardData(completionHandler: { (data) in
            self.cardData = data
            self.bankTable.reloadData()
        })
    }
    
    
    //MARK: IBACtions
    @IBAction func addCardAction(_ sender: Any) {
        if(self.addCardButton.titleLabel?.text == "Add"){
            self.addNewCardAction()
        }else {
            self.addCardButton.backgroundColor = K_PRIMARY_COLOR
            self.addMoneyButton.backgroundColor = .lightGray
            self.bankCardView.isHidden = true
            self.addCardView.isHidden = false
            self.addMoneyButton.setTitle("+ Money to Wallet", for: .normal)
            self.addCardButton.setTitle("Add", for: .normal)
        }
    }
    
    @IBAction func addMoneyAction(_ sender: Any) {
        if(self.addMoneyButton.titleLabel?.text == "Add"){
            self.addMoneyAction()
        }else {
            self.addCardButton.backgroundColor = .lightGray
            self.addMoneyButton.backgroundColor = K_PRIMARY_COLOR
            self.bankCardView.isHidden = false
            self.addCardView.isHidden = true
            self.addMoneyButton.setTitle("Add", for: .normal)
            self.addCardButton.setTitle("+ Card", for: .normal)
        }
    }
    
    func addNewCardAction() {
        if(cardNumber.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card number")
        }else if(cardName.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card name")
        }else if(monthField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card expiry month")
        }else if(yearField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card expiry year")
        }else if(cvvField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter cvv number")
        }else {
            ActivityIndicator.show(view: self.view)
            let card = self.cardNumber.text?.replacingOccurrences(of: " ", with: "")
            let param = [
                "card_name":self.cardName.text,
                "expiry_month":self.monthField.text,
                "expiry_year":self.yearField.text,
                "card_number":card,
                "cvc":self.cvvField.text,
                "payment_gateway":1
            ] as! [String:Any]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CARD, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_CARD) { (response) in
                Singleton.shared.showToast(text: "Successfully added card")
                self.initView()
                Singleton.shared.cardData = []
                ActivityIndicator.hide()
                self.callAPI()
            }
        }
    }
    
    
    func addMoneyAction() {
        self.walletAmount.resignFirstResponder()
        if(self.walletAmount.text!.isEmpty){
            Singleton.shared.showToast(text: "Please enter Amount.")
        }else if(Singleton.shared.cardData.count == 0){
            Singleton.shared.showToast(text: "Please add card.")
        }else {
            let param: [String:Any] = [
                "payment_method":Singleton.shared.cardData[0].id,
                "amount":self.walletAmount.text
            ]
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_MONEY_WALLET, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_MONEY_WALLET) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.walletData = []
                Singleton.shared.transactionData = []
                self.initView()
                Singleton.shared.showToast(text: "Money added to wallet.")
                self.callAPI()
            }
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        //  let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SuccessScreenViewController") as! SuccessScreenViewController
        //self.navigationController?.pushViewController(myVC, animated: true)
        
        self.dismiss(animated: false, completion: nil)
        if(self.isWalletSelected){
            self.cardDelegate?.selectedCardDetail(type: 1, card: GetCardResponse())
        }else {
            self.cardDelegate?.selectedCardDetail(type: 2, card: selectedCard)
        }
        
    }
    
}

extension SelectCardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BankCardCell") as! BankCardCell
        
        if(indexPath.row == (self.cardData.count)){
            cell.view1.isHidden = true
            cell.view2.isHidden = false
            if(walletData.count > 0){
                cell.walletBalance.text = "Available balance: " +
                    (self.walletData[0].symbol ?? "") + (self.walletData[0].balance ?? "0.0")
            }
            if(self.isWalletSelected){
                cell.tickImage2.backgroundColor = K_PRIMARY_COLOR
            }else{
                cell.tickImage2.backgroundColor = .white
            }
        }else {
            cell.view1.isHidden = false
            cell.view2.isHidden = true
            let val = self.cardData[indexPath.row]
            if(self.isWalletSelected == false && self.selectedCard.id == val.id){
                cell.tickImage1.backgroundColor = K_PRIMARY_COLOR
            }else{
                cell.tickImage1.backgroundColor = .white
            }
            
            cell.cardImage.image = #imageLiteral(resourceName: "mastercard")
            cell.cardNumber.text = val.masked_card_number
            cell.cardExpiry.setTitle("\(val.expiry_month ?? "")/\(val.expiry_year ?? "")", for: .normal)
        }
        
        cell.selectCard = {
            if !(self.isWalletSelected) {
                self.selectedCard = self.cardData[indexPath.row]
                self.bankTable.reloadData()
            }
        }
        
        cell.selectWallet = {
            self.isWalletSelected = true
            self.selectedCard = GetCardResponse()
            self.bankTable.reloadData()
        }
        
        return cell
    }
}

extension SelectCardViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == cardNumber){
            let currentCharacterCount = self.cardNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.cardNumber.text = self.cardNumber.text?.applyPatternOnNumbers(pattern: "#### #### #### #######", replacmentCharacter: "#")
            return newLength <= 22
        }else if(textField == monthField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 2) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == yearField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 4) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == cvvField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count <= 3) {
                    return true
                }else {
                    return false
                }
            }
        }
        return true
    }
}

class BankCardCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var cardNumber: DesignableUILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardExpiry: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var walletBalance: DesignableUILabel!
    @IBOutlet weak var tickImage2: ImageView!
    @IBOutlet weak var tickImage1: ImageView!
    
    var selectCard:(()-> Void)? = nil
    var selectWallet:(()-> Void)? = nil
    
    
    //MARK: IBActions
    @IBAction func tickAction(_ sender: Any) {
        if let selectCard = self.selectCard {
            selectCard()
        }
    }
    
    @IBAction func walletAction(_ sender: Any) {
        if let selectWallet = self.selectWallet {
            selectWallet()
        }
    }
}
