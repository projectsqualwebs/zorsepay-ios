//
//  SendViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class SendViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var firstSelectionView: View!
    @IBOutlet weak var secondSelectionView: View!
    @IBOutlet weak var thirdSelectionView: View!
    
    var userData = TransactionResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func handleTextChange(val: String){
        self.firstSelectionView.backgroundColor = .clear
        self.secondSelectionView.backgroundColor = .clear
        self.thirdSelectionView.backgroundColor = .clear
        if(self.amountLabel.text == "$0" || self.amountLabel.text == ""){
            self.amountLabel.text =  "$" + val
        }else {
            self.amountLabel.text =  (self.amountLabel.text ?? "") + val
        }
    }
    
    //MARK:IBActions
    @IBAction func oneAction(_ sender: Any) {
        self.handleTextChange(val: "1")
    }
    
    @IBAction func twoAction(_ sender: Any) {
        self.handleTextChange(val: "2")
    }
    
    @IBAction func threeAction(_ sender: Any) {
        self.handleTextChange(val: "3")
    }
    
    @IBAction func fourAction(_ sender: Any) {
        self.handleTextChange(val: "4")
    }
    
    @IBAction func fiveAction(_ sender: Any) {
        self.handleTextChange(val: "5")
    }
    
    @IBAction func sixAction(_ sender: Any) {
        self.handleTextChange(val: "6")
    }
    
    @IBAction func sevenAction(_ sender: Any) {
        self.handleTextChange(val: "7")
    }
    
    @IBAction func eightAction(_ sender: Any) {
        self.handleTextChange(val: "8")
    }
    
    @IBAction func nineAction(_ sender: Any) {
        self.handleTextChange(val: "9")
    }
    
    @IBAction func zeroAction(_ sender: Any) {
        self.handleTextChange(val: "0")
    }
    
    @IBAction func clearAction(_ sender: Any) {
        if(self.amountLabel.text!.isEmpty){
            return
        }
        self.amountLabel.text?.removeLast()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func sendAction(_ sender: Any) {
        if(self.amountLabel.text!.isEmpty || self.amountLabel.text == "$0"){
            Singleton.shared.showToast(text: "Enter amount to send")
        }else{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TransferViewController") as! TransferViewController
            if(self.userData.receiver_id == Singleton.shared.userDetail.id){
                myVC.selectedUser = SearchUserResponse(id: self.userData.receiver_id, first_name: self.userData.receiver_name, profile_image: self.userData.receiver_profile_image, email: self.userData.receiver_email, phone_number: self.userData.receiver_phone_number, service_station: "", country_name: "")
            }else {
                myVC.selectedUser = SearchUserResponse(id: self.userData.sender_id, first_name: self.userData.sender_name, profile_image: self.userData.sender_profile_image, email: self.userData.sender_email, phone_number: self.userData.sender_phone_number, service_station: "", country_name: "")
            }
            myVC.amount = (self.amountLabel.text?.replacingOccurrences(of: "$", with: ""))!
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @IBAction func requestAction(_ sender: Any) {
        if(self.amountLabel.text!.isEmpty || self.amountLabel.text == "$0"){
            Singleton.shared.showToast(text: "Enter amount to send")
        }else{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            myVC.isRequestMoney = true
            myVC.amount = (self.amountLabel.text?.replacingOccurrences(of: "$", with: ""))!
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }
    
    @IBAction func firstSelectionAction(_ sender: Any) {
        self.amountLabel.resignFirstResponder()
        self.firstSelectionView.backgroundColor = .white
        self.secondSelectionView.backgroundColor = .clear
        self.thirdSelectionView.backgroundColor = .clear
        self.amountLabel.text = "$100"
    }
    
    @IBAction func secondSelectionAction(_ sender: Any) {
        self.amountLabel.resignFirstResponder()
        self.secondSelectionView.backgroundColor = .white
        self.firstSelectionView.backgroundColor = .clear
        self.thirdSelectionView.backgroundColor = .clear
        self.amountLabel.text = "$300"
    }
    
    @IBAction func thirdSelectionAction(_ sender: Any) {
        self.amountLabel.resignFirstResponder()
        self.thirdSelectionView.backgroundColor = .white
        self.secondSelectionView.backgroundColor = .clear
        self.firstSelectionView.backgroundColor = .clear
        self.amountLabel.text = "$500"
        
    }
    
}
