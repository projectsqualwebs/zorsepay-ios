//
//  SignupViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
//import CountryPickerView
import SkyFloatingLabelTextField


class SignupViewController: UIViewController, UITextFieldDelegate, SelectFromPicker {
    func selectedItem(name: String, id: Int) {
        self.selectedCountry = self.countryData[id]
        self.countryField.text = name
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var signupLabel: DesignableUILabel!
    @IBOutlet weak var firstNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var firstNameImage: UIImageView!
    @IBOutlet weak var lastNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameImage: UIImageView!
    @IBOutlet weak var numberTitle: DesignableUILabel!
    @IBOutlet weak var numberField: SkyFloatingLabelTextField!
    @IBOutlet weak var numberImage: UIImageView!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var countryField: SkyFloatingLabelTextField!
    
    // @IBOutlet weak var countryPickerView: CountryPickerView!
    @IBOutlet weak var registerButton: CustomButton!
    @IBOutlet weak var termsText: DesignableUILabel!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    // @IBOutlet weak var addressField: SkyFloatingLabelTextField!
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextField!
    
    var countryData = [Countries]()
    var selectedCountry = Countries()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        NavigationController.shared.getCountryData { response in
            self.countryData = response
        }
        //self.countryPickerView.showCountryCodeInView = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initView()
    }
    
    func initView(){
        
        self.firstNameField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.firstName.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.lastName.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.numberField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.emailField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.passwordField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.countryField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.confirmPassword.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.firstNameField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        
        self.firstNameField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.firstName.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.lastName.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.numberField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.countryField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.emailField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.passwordField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.confirmPassword.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.firstNameField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        
        self.signupLabel.text = "SIGNUP"
        //  self.numberTitle.text = "Your Phone"
        self.registerButton.setTitle("Create account", for: .normal)
        self.firstNameField.delegate  = self
        // self.lastNameField.delegate = self
        self.emailField.delegate = self
        self.numberField.delegate = self
        self.passwordField.delegate = self
        self.confirmPassword.delegate = self
    }
    
    //MARK: IBActions
    
    @IBAction func registerAction(_ sender: Any) {
        if(self.firstNameField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter username.")
        }else if(self.firstName.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter First Name.")
        }else if(self.lastName.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Last Name.")
        }else if(self.emailField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Email Address.")
        }else if !(self.isValidEmail(emailStr: self.emailField.text ?? "")){
            Singleton.shared.showToast(text: "Enter valid Email Address.")
        }else if(self.countryField.text!.isEmpty){
            Singleton.shared.showToast(text: "Select country")
        }else if(self.numberField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Phone Number.")
        }else if(self.passwordField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter password.")
        }else if(self.confirmPassword.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter confirm password.")
        }else if(self.confirmPassword.text != self.passwordField.text){
            Singleton.shared.showToast(text: "Password didn't matched")
        }else{
            ActivityIndicator.show(view: self.view)
            let number = self.numberField.text?.replacingOccurrences(of: "-", with: "")
            let token = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
            let param:[String:Any] = [
                "username": self.firstNameField.text ?? "",
                "first_name": self.firstName.text ?? "",
                "last_name": self.lastName.text ?? "",
                "email": self.emailField.text ?? "",
                "phone_number":number,
                "password": self.passwordField.text ?? "",
                "token":token ?? "",
                "platform":2,
                "country_id": self.selectedCountry.id
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_REGISTER, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_REGISTER) { (response) in
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MobileVerificationViewController") as! MobileVerificationViewController
                UserDefaults.standard.set(self.emailField.text ?? "", forKey: UD_USER_EMAIL)
                UserDefaults.standard.set("MobileVerificationViewController", forKey: UD_INITIAL_VIEW_CONTROLLER)
                myVC.email = self.emailField.text ?? ""
                ActivityIndicator.hide()
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
        
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        myVC.backButtonVisible = false
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        for val in self.countryData{
            myVC.pickerData.append(val.country ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.present(myVC, animated: true, completion: nil)
        }
    }
    
}

extension SignupViewController {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == numberField){
            let currentCharacterCount = self.numberField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.numberField.text = self.numberField.text?.applyPatternOnNumbers(pattern: "###-####-#####", replacmentCharacter: "#")
            return newLength <= 14
        }else {
            return true
        }
    }
}
