//
//  ForgotPasswordViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPasswordViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var otpView: View!
    @IBOutlet weak var passwordView: View!
    @IBOutlet weak var otpField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var sendButton: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.passwordField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.otpField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        
        self.emailField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.passwordField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.otpField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        
    }
    
    //MARK: IBActions
    @IBAction func sendAction(_ sender: Any) {
        if(self.sendButton.titleLabel?.text == "Send"){
            if(self.emailField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Email Address")
            }else if !(self.isValidEmail(emailStr: self.emailField.text ?? "")){
                Singleton.shared.showToast(text: "Enter valid Email Address")
            }else {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_FORGOT_PASS, method: .post, parameter: ["email":self.emailField.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_FORGOT_PASS) { (response) in
                    ActivityIndicator.hide()
                    self.otpView.isHidden = false
                    self.sendButton.setTitle("Verify OTP", for: .normal)
                    Singleton.shared.showToast(text: response.message ?? "")
                }
            }
        }else if(self.sendButton.titleLabel?.text == "Verify OTP"){
            if(self.otpField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter OTP")
            }else {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_VERIRFY_FORGET_PASS_OTP, method: .post, parameter: ["email":self.emailField.text ?? "", "otp": self.otpField.text ?? ""], objectClass: SuccessResponse.self, requestCode: U_VERIRFY_FORGET_PASS_OTP) { (response) in
                    ActivityIndicator.hide()
                    self.sendButton.setTitle("Reset Password", for: .normal)
                    self.passwordView.isHidden = false
                    Singleton.shared.showToast(text: response.message ?? "")
                }
            }
        }else if(self.sendButton.titleLabel?.text == "Reset Password"){
            if(self.passwordField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Password")
            }else {
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_RESET_PASSWORD, method: .post, parameter: ["email":self.emailField.text ?? "", "otp": self.otpField.text ?? "", "password": self.passwordField.text ?? "" ], objectClass: SuccessResponse.self, requestCode: U_RESET_PASSWORD) { (response) in
                    ActivityIndicator.hide()
                    Singleton.shared.showToast(text: response.message ?? "")
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

