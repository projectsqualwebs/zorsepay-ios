//
//  LoginViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

var dontRememberMe = false

class LoginViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var signinLabel: DesignableUILabel!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var bottomText1: DesignableUILabel!
    @IBOutlet weak var bottomText2: DesignableUILabel!
    @IBOutlet weak var forgotPasswordText: DesignableUILabel!
    @IBOutlet weak var rememberView: View!
    @IBOutlet weak var loginView: UIView!
    
    var backButtonVisible = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.passwordField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        
        self.emailField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.passwordField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        
        self.loginView.isHidden = backButtonVisible
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if vc.isKind(of: LoginViewController.self) {
                return false
            } else {
                return true
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
       initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.signinLabel.text = ""
        self.passwordField.text = ""
    }
    
    func initView(){
        self.signinLabel.text = "LOGIN"
        self.passwordField.isSecureTextEntry = true
        self.loginButton.setTitle("Login", for: .normal)
        self.emailField.delegate = self
        self.passwordField.delegate = self
        self.rememberView.borderWidth = 0.5
        self.rememberView.borderColor = K_YELLOW_COLOR
        self.rememberView.backgroundColor = .white
    }
    
    //MARK: IBActions
    @IBAction func loginAction(_ sender: Any) {
        if(self.emailField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Email Address.")
        }else if !(self.isValidEmail(emailStr: self.emailField.text ?? "")){
            Singleton.shared.showToast(text: "Enter valid Email Address.")
        }else if(self.passwordField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter password.")
        }else {
            ActivityIndicator.show(view: self.view)
            let token = UserDefaults.standard.value(forKey: UD_FCM_TOKEN) as? String
            let param:[String:Any] = [
                "email": self.emailField.text ?? "",
                "password": self.passwordField.text ?? "",
                "token":token ?? "",
                "platform":2
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN, method: .post, parameter: param, objectClass: Signup.self, requestCode: U_LOGIN) { (response) in
                UserDefaults.standard.setValue(response.response.token, forKey: UD_TOKEN)
                Singleton.shared.userDetail = response.response
                if(self.rememberView.backgroundColor == K_PRIMARY_COLOR){
                  let userData = try! JSONEncoder().encode(response.response)
                  UserDefaults.standard.set(userData, forKey: UD_USER_DETAIL)
                }else {
                    dontRememberMe = true
                }
                ActivityIndicator.hide()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rememberAction(_ sender: Any) {
        if(self.rememberView.backgroundColor == .white){
           self.rememberView.backgroundColor = K_YELLOW_COLOR
           self.rememberView.borderColor = K_YELLOW_COLOR
        }else{
        self.rememberView.backgroundColor = .white
        self.rememberView.borderColor = K_YELLOW_COLOR
        }
    }
    
    @IBAction func showPasswordAction(_ sender: Any) {
        if(self.passwordField.isSecureTextEntry){
            self.passwordField.isSecureTextEntry = false
        }else{
            self.passwordField.isSecureTextEntry = true
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
   
}
