//
//  TrasnasctionViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class TrasnasctionViewController: UIViewController, UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.searchText = (textField.text ?? "") + string
        self.searchData = []
        self.searchData = self.transactionData.filter{
            
                ($0.sender_name ?? "").lowercased().contains(self.searchText.lowercased()) || ($0.receiver_name ?? "").lowercased().contains(self.searchText.lowercased())
            
        }
        if(self.searchText == ""){
            self.searchData = []
        }
        self.transactionTable.reloadData()
        return true
    }
    
    
    //MARK: IBOutlets
   // @IBOutlet weak var searchFIeld: CustomTextField!
    @IBOutlet weak var searchPlaceholder: DesignableUILabel!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var transactionTable: ContentSizedTableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var noTransactionLabel: UIStackView!
    @IBOutlet weak var amountView: UIStackView!
    
    @IBOutlet weak var amountRequested: DesignableUILabel!
    @IBOutlet weak var amountReceived: DesignableUILabel!
    
    
    var transactionData = [TransactionResponse]()
    var searchData = [TransactionResponse]()
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transactionTable.tableFooterView = UIView()
        self.searchField.delegate = self
        searchIcon.image = #imageLiteral(resourceName: "mysearch")
      //  self.searchFIeld.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getTransactionData { (data) in
            self.transactionData = data
            var requested = 0.0
            var received = 0.0
            for val in self.transactionData{
                if(val.transaction_type == 2){
                    requested += Double(val.amount ?? "0")!
                }else if(val.transaction_type == 3){
                    received += Double(val.amount ?? "0")!
                }
            }
            self.amountRequested.text = "$\(requested)"
            self.amountReceived.text = "$\(received)"
            self.transactionTable.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.userImage.sd_setImage(with: URL(string:self.getImageURL(text:Singleton.shared.userDetail.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
    }
    
    //MARK: IBActions
    @IBAction func profileAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FilterTransactionViewController") as! FilterTransactionViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        //        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchBarViewController") as! SearchBarViewController
        //        self.navigationController?.pushViewController(myVC, animated: true)
        if(searchIcon.image == #imageLiteral(resourceName: "mysearch")){
            searchIcon.image = #imageLiteral(resourceName: "clear")
            self.searchField.text = ""
            self.searchText = ""
            self.searchData = []
            self.searchField.becomeFirstResponder()
            self.transactionTable.reloadData()
            self.searchField.isHidden = false
        }else {
            searchIcon.image = #imageLiteral(resourceName: "mysearch")
            self.searchField.text = ""
            
            self.searchText = ""
            self.searchData = []
            self.searchField.resignFirstResponder()
            self.transactionTable.reloadData()
            self.searchField.isHidden = true
        }
    }
}

extension TrasnasctionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.searchData.count > 0){
            return self.searchData.count
        }else {
            if(self.transactionData.count == 0){
                self.noTransactionLabel.isHidden = false
                self.amountView.isHidden = true
            }else {
                self.noTransactionLabel.isHidden = true
                self.amountView.isHidden = false
            }
            return self.transactionData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        var val = TransactionResponse()
        if(self.searchData.count > 0){
            val = self.searchData[indexPath.row]
        }else {
            val = self.transactionData[indexPath.row]
        }
        if(val.receiver_id == Singleton.shared.userDetail.id){
            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.userName.text = val.sender_name
            
        }else {
            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.userName.text = val.receiver_name ?? "You"
        }
        if(val.transaction_type == 2){
            cell.transactionAmount.textColor = K_RED_COLOR
            cell.transactionAmount.text = "-" + (val.amount ?? "")
        }else {
            cell.transactionAmount.textColor = K_GREEN_COLOR
            cell.transactionAmount.text = "+" + (val.amount ?? "")
        }
        cell.transactionType.text = val.transaction_method ?? "Stripe"
        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        return cell
    }
}

