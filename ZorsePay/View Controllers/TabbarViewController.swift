//
//  TabbarViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit


class TabbarViewController: UIViewController {
    //MARK: IBOutlet
    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var homeButton: CustomButton!
 //   @IBOutlet weak var homeSelectionView: View!
    
    @IBOutlet weak var transactionImage: UIImageView!
    @IBOutlet weak var transactionButton: CustomButton!
  //  @IBOutlet weak var transactionSelectionView: View!
    
    @IBOutlet weak var sendImage: UIImageView!
   // @IBOutlet weak var sendSelectionView: View!
    
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactButton: CustomButton!
   // @IBOutlet weak var contactSelectionView: View!
    
    @IBOutlet weak var walletImage: UIImageView!
    @IBOutlet weak var walletButton: CustomButton!
   // @IBOutlet weak var walletSelectionView: View!
    
    var currentIndex = Int()
    var userData = TransactionResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView(image: self.homeImage)
        self.homeAction(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSelectedTab(_:)), name: NSNotification.Name("change_tab"), object: nil)
    }
    
    func initView(image: UIImageView){
        self.homeImage.changeTint(color: K_WHITE_COLOR)
        self.transactionImage.changeTint(color: K_WHITE_COLOR)
        self.sendImage.changeTint(color: K_WHITE_COLOR)
        self.contactImage.changeTint(color: K_WHITE_COLOR)
        self.walletImage.changeTint(color: K_WHITE_COLOR)
        image.changeTint(color:K_YELLOW_COLOR)
    }
    
    
    @objc func showSelectedTab(_ notif: Notification) {
        if let tab = notif.userInfo?["tab"] as? String {
            if(tab == "1"){
                self.transactionAction(self)
            }else if(tab == "2"){
                self.userData = notif.userInfo?["userData"] as! TransactionResponse
                self.sendAction(self)
            }
        }
        
    }
    
    //MARK: IBActions
    @IBAction func homeAction(_ sender: Any) {
        self.initView(image: self.homeImage)
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerFirst()
        
    }
    
    @IBAction func transactionAction(_ sender: Any) {
        self.initView(image: self.transactionImage)
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerSecond()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        self.initView(image: self.sendImage)
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerThird()

    }
    
    @IBAction func contactAction(_ sender: Any) {
        self.initView(image: self.contactImage)
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerFourth()
    }
    
    @IBAction func walletAction(_ sender: Any) {
        self.initView(image: self.walletImage)
        let pageVC = TabPageViewController.dataSource1 as? TabPageViewController
        pageVC?.setControllerLast()
    }
    
}




