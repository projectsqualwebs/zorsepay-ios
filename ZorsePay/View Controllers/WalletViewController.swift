//
//  WalletViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var transactionTable: ContentSizedTableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var noTransactionLabel: UIStackView!
    @IBOutlet weak var walletBalance: DesignableUILabel!
    
    var transactionData = [TransactionResponse]()
    var walletData = [WalletResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transactionTable.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getTransactionData { (data) in
            self.transactionData = data
            self.transactionTable.reloadData()
        }
        
        NavigationController.shared.getWalletData(completionHandler: { (data) in
            self.walletData = data
            if(data.count > 0){
            self.walletBalance.text = (self.walletData[0].symbol ?? "") + (self.walletData[0].balance ?? "0.0")
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.userImage.sd_setImage(with: URL(string:self.getImageURL(text:Singleton.shared.userDetail.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
       }
       
    
    //MARK: IBActions
     @IBAction func profileAction(_ sender: Any) {
         let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
         self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addWalletAction(_ sender: UIButton) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyViewController") as! AddMoneyViewController
        myVC.actionType = sender.tag 
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}


extension WalletViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            if(self.transactionData.count == 0){
                self.noTransactionLabel.isHidden = false
            }else {
                self.noTransactionLabel.isHidden = true
            }
            return self.transactionData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        var val =  self.transactionData[indexPath.row]
        if(val.receiver_id == Singleton.shared.userDetail.id){
            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.userName.text = val.sender_name

        }else {
            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.userName.text = val.receiver_name ?? "You"
        }
        if(val.transaction_type == 2){
            cell.transactionAmount.textColor = K_RED_COLOR
        }else {
            cell.transactionAmount.textColor = K_GREEN_COLOR
        }
        cell.transactionAmount.text = val.amount
        cell.transactionType.text = val.transaction_method ?? "Stripe"
        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        
        return cell
    }
}
