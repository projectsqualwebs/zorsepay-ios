//
//  ScanDetailViewController.swift
//
//  Created by qw on 27/09/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class ScanDetailViewController: UIViewController {
    //MARK: IBOutlets
    
    @IBOutlet weak var userImage: ImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.userImage.sd_setImage(with: URL(string:self.getImageURL(text:Singleton.shared.userDetail.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
    }
    
    //MARK: IBAction
    @IBAction func scanAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
