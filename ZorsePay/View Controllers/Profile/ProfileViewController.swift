//
//  ProfileViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import  SDWebImage

class ProfileViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var userId: DesignableUILabel!
    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var scanImage: UIImageView!
    
    
    var profileData = SignupResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scanImage.sd_setImage(with: URL(string:U_QR_BASE_URL + (Singleton.shared.userDetail.qr_code ?? "")), placeholderImage: nil)
    }
  
    
    override func viewDidAppear(_ animated: Bool) {
        self.profileData = Singleton.shared.userDetail
        self.userImage.sd_setImage(with: URL(string:self.getImageURL(text:Singleton.shared.userDetail.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
        self.userId.text = "ID-MT" + "\(Singleton.shared.userDetail.id ?? 0)"
    }
    
    //MARK: IBActions
    @IBAction func settingsAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func securityAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        myVC.isChangeSecurityPin = true
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func logoutAction(_ sender: Any) {
      let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
      let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        UserDefaults.standard.removeObject(forKey: UD_USER_EMAIL)
        UserDefaults.standard.removeObject(forKey: UD_USER_DETAIL)
        Singleton.shared.cardData = []
        Singleton.shared.paymentMethods = []
        Singleton.shared.currencyData = []
        Singleton.shared.walletData = []
        Singleton.shared.transactionData = []
        Singleton.shared.contactData = []
        Singleton.shared.countryData = []
      self.navigationController?.pushViewController(myVC, animated: true)
        }
        let action2 = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func profileDetailAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetialViewController") as! ProfileDetialViewController
        myVC.profileData = self.profileData
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func qrCodeAction(_ sender: Any) {
        self.scanView.isHidden = false
    }
    
    @IBAction func profileVerification(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanDetailViewController") as! ScanDetailViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        NavigationController.shared.pushDashboard(controller:self)
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func helpSupportAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "HelpSupportViewController") as! HelpSupportViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func referAppAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferFriendViewController") as! ReferFriendViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func paymentMethodAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.scanView.isHidden = true
    }
    
    
}
