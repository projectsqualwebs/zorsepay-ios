//
//  NotificationViewController.swift
//  ZorsePay
//
//  Created by Sagar Pandit on 08/07/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var notificationTable: ContentSizedTableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        return cell
    }
    
    
}
