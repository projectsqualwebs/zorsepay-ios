//
//  ContactViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         
        self.searchText = (textField.text ?? "") + string
        self.searchData = []
        self.searchData = self.contactData.filter{
            ($0.sender_name ?? "").lowercased().contains(self.searchText.lowercased()) || ($0.receiver_name ?? "").lowercased().contains(self.searchText.lowercased())
        }
        if(self.searchText == ""){
            self.searchData = []
        }
        self.contactTable.reloadData()
        return true
        
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var contactTable: ContentSizedTableView!
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var scanImage: UIImageView!
    @IBOutlet weak var scanHeight: NSLayoutConstraint!
 //   @IBOutlet weak var referFriendButton: UIButton!
    @IBOutlet weak var noContactLabel: UIStackView!
    
    
    var contactData = [TransactionResponse]()
    var searchData = [TransactionResponse]()
    var amount = String()
    var isRequestMoney = false
    var searchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchField.delegate = self
        contactTable.tableFooterView = UIView()
        searchIcon.image = #imageLiteral(resourceName: "mysearch")
        if(self.isRequestMoney){
            self.scanImage.image = #imageLiteral(resourceName: "icon_back")
            self.scanHeight.constant  = 15
          //  self.referFriendButton.isHidden = true
        }else {
          //  self.referFriendButton.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getContactData { (data) in
            self.contactData = data
            self.contactTable.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.userImage.sd_setImage(with: URL(string:self.getImageURL(text:Singleton.shared.userDetail.profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
    }

    
    //MARK: IBActions
     @IBAction func userAction(_ sender: Any) {
         let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
         self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func referFriendAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferFriendViewController") as! ReferFriendViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    

    
    @IBAction func scanAction(_ sender: Any) {
        if(self.isRequestMoney){
            self.navigationController?.popViewController(animated: true)
        }
    }
   
    @IBAction func searchAction(_ sender: Any) {
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchBarViewController") as! SearchBarViewController
//        self.navigationController?.pushViewController(myVC, animated: true)
        if(searchIcon.image == #imageLiteral(resourceName: "mysearch")){
          searchIcon.image = #imageLiteral(resourceName: "clear")
            self.searchField.becomeFirstResponder()
            self.searchData = []
            self.searchField.text = ""
            self.searchText = ""
            self.contactTable.reloadData()
            self.searchField.isHidden = false
        }else {
          searchIcon.image = #imageLiteral(resourceName: "mysearch")
            self.searchField.text = ""
            self.searchText = ""
            self.searchData = []
            self.contactTable.reloadData()
            self.searchField.resignFirstResponder()
            self.searchField.isHidden = true
        }
    }
  
    
}

extension ContactViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.searchData.count > 0){
           return self.searchData.count
        }else {
            if(self.contactData.count == 0){
                self.noContactLabel.isHidden = false
            }else {
                self.noContactLabel.isHidden = true
            }
           return contactData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
        var val = TransactionResponse()
        if(self.searchData.count > 0){
            val = self.searchData[indexPath.row]
        }else {
            val = self.contactData[indexPath.row]
        }
            
        var requested_user_id = Int()
        if(self.isRequestMoney){
            cell.sendBtn.setTitle("Request", for: .normal)
        }
        if(val.receiver_id == Singleton.shared.userDetail.id){
            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.userName.text = val.sender_name
            cell.transactionDate.text = (val.sender_phone_number ?? "").applyPatternOnNumbers(pattern: "###-####-#####", replacmentCharacter: "#")
            requested_user_id = val.sender_id ?? 0
                
                        
        }else {
            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
            cell.userName.text = val.receiver_name ?? "You"
            cell.transactionDate.text = (val.receiver_phone_number ?? "").applyPatternOnNumbers(pattern: "###-####-#####", replacmentCharacter: "#")
            requested_user_id = val.receiver_id ?? 0
        }
        
        cell.sendButton = {
            if(self.isRequestMoney){
                let alert = UIAlertController(title: "Request Money", message: "send reqest to \(cell.userName.text ?? "") for $\(self.amount) ", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Yes", style: .default) { (action) in
                    let param: [String:Any] = [
                        "requested_user_id": requested_user_id,
                        "amount": self.amount,
                        "currency_id": Singleton.shared.userDetail.currency_id ?? 0
                    ]
                    
                    ActivityIndicator.show(view: self.view)
                    SessionManager.shared.methodForApiCalling(url: U_BASE + U_REQUEST_MONEY, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_REQUEST_MONEY) { (response) in
                        Singleton.shared.showToast(text: "Successfully sent request.")
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                        self.navigationController?.pushViewController(myVC, animated: true)
                    }
                }
                
                let action2 = UIAlertAction(title: "No", style: .default) { (action) in
                    self.dismiss(animated: false, completion: nil)
                }
                
                alert.addAction(action1)
                alert.addAction(action2)
                self.present(alert, animated: true, completion: nil)
            }else {
             NotificationCenter.default.post(name: NSNotification.Name("change_tab"), object: nil,userInfo: ["tab":"2", "userData": val])
            }
        }
     
        return cell
    }
}
