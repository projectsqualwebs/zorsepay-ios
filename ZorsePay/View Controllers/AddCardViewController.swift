//
//  AddCardViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class AddCardViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var cardName: SkyFloatingLabelTextField!
    @IBOutlet weak var cardNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var cvvField: SkyFloatingLabelTextField!
    @IBOutlet weak var monthField: SkyFloatingLabelTextField!
    @IBOutlet weak var yearField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView(){
        self.cardNumber.delegate = self
        self.monthField.delegate = self
        self.yearField.delegate = self
        self.cvvField.delegate = self
        
        self.cardName.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.monthField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.yearField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.cardNumber.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        self.cvvField.titleFont = UIFont(name: "OpenSans-Regular", size: 10)!
        
        self.cardName.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.monthField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.yearField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.cardNumber.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
        self.cvvField.placeholderFont = UIFont(name: "OpenSans-Regular", size: 12)!
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        if(cardNumber.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card number")
        }else if(cardName.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card name")
        }else if(monthField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card expiry month")
        }else if(yearField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter card expiry year")
        }else {
            ActivityIndicator.show(view: self.view)
            let card = self.cardNumber.text?.replacingOccurrences(of: " ", with: "")
            let param = [
                "card_name":self.cardName.text,
                "expiry_month":self.monthField.text,
                "expiry_year":self.yearField.text,
                "card_number":card,
                "cvc":self.cvvField.text ?? "123",
               "payment_gateway":1,
                "is_default":1,
                ] as! [String:Any]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_CARD, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_ADD_CARD) { (response) in
                Singleton.shared.showToast(text: "Successfully added card")
                Singleton.shared.cardData = []
                ActivityIndicator.hide()
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
}

extension AddCardViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == cardNumber){
            let currentCharacterCount = self.cardNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.cardNumber.text = self.cardNumber.text?.applyPatternOnNumbers(pattern: "#### #### #### #######", replacmentCharacter: "#")
            return newLength <= 22
        }else if(textField == monthField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 2) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == yearField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 4) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == cvvField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count <= 3) {
                    return true
                }else {
                    return false
                }
            }
        }
        return true
    }
}
