//
//  AddCardViewController.swift
//
//  Created by qw on 23/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import TransitionButton

class AddMoneyViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var confirmButton: TransitionButton!
    @IBOutlet weak var cardCollection: UICollectionView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var firstSelectionView: View!
    @IBOutlet weak var secondSelectionView: View!
    @IBOutlet weak var thirdSelectionView: View!
    @IBOutlet weak var currentBalance: DesignableUILabel!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var amountToWithdrawLabel: DesignableUILabel!
    @IBOutlet weak var quickAmountView: View!
    @IBOutlet weak var selectPaymentMethodLabel: DesignableUILabel!
    @IBOutlet weak var viewForCard: UIView!
    @IBOutlet weak var transferButtonView: UIStackView!
    
    
    var amount = String()
    var cardData = [GetCardResponse]()
    var selectedCard = Int()
    var actionType = 1
    var emailID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.amountField.delegate = self
        self.confirmButton.layer.cornerRadius = 20
        self.amountField.becomeFirstResponder()
        if(self.actionType == 2){
            self.viewForCard.isHidden = true
            self.headingLabel.text = "Withdraw Money"
          //  self.amountToWithdrawLabel.text = "Enter amount to withdraw"
            self.quickAmountView.isHidden = true
            self.selectPaymentMethodLabel.text = "Select a card for withdrawal amount"
            self.confirmButton.setTitle("Withdraw money from wallet", for: .normal)
        }else if(self.actionType == 3){
            self.transferButtonView.isHidden = false
            self.confirmButton.isHidden = true
            self.headingLabel.text = "Payment details"
            self.selectPaymentMethodLabel.text = "Select a card."
            self.confirmButton.setTitle("Withdraw money from wallet", for: .normal)
        }
        amountField.attributedPlaceholder = NSAttributedString(string: "$0",
                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NavigationController.shared.getCardData { (data) in
            self.cardData = data
            self.cardCollection.reloadData()
        }
        
        NavigationController.shared.getWalletData { (data) in
            if(data.count > 0){
                self.currentBalance.text = "Current Balance: " +  "\(data[0].symbol ?? "")\(data[0].balance ?? "")"
            }
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == cardCollection){
            for cell in cardCollection.visibleCells {
                let indexPath = cardCollection.indexPath(for: cell)
               
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func addMoneyAction(_ sender: Any) {
        if(self.actionType == 2){
            Singleton.shared.showToast(text: "Please submit your bank details then withdraw the amount.")
            return
        }
        self.amountField.resignFirstResponder()
        if(self.amount == ""){
            Singleton.shared.showToast(text: "Please enter Amount.")
        }else if(self.selectedCard == 0){
            Singleton.shared.showToast(text: "Please select card.")
        }else {
            
            let param: [String:Any] = [
                "payment_method":self.selectedCard,
                "amount":self.amount.replacingOccurrences(of: "$", with: "")
            ]
            var url = String()
            
            if(self.actionType == 2){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PincodeViewController") as! PincodeViewController
                myVC.param = param
                myVC.selectedMethod = 4
                self.navigationController?.pushViewController(myVC, animated: true)
                return
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PincodeViewController") as! PincodeViewController
                myVC.param = param
                myVC.selectedMethod = 5
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func firstSelectionAction(_ sender: Any) {
        self.amountField.resignFirstResponder()
        self.firstSelectionView.backgroundColor = .white
        self.secondSelectionView.backgroundColor = .clear
        self.thirdSelectionView.backgroundColor = .clear
        self.amount = "100"
        self.amountField.text = "$100"
    }
    
    @IBAction func secondSelectionAction(_ sender: Any) {
        self.amountField.resignFirstResponder()
        self.secondSelectionView.backgroundColor = .white
        self.firstSelectionView.backgroundColor = .clear
        self.thirdSelectionView.backgroundColor = .clear
        self.amount = "300"
        self.amountField.text = "$300"
    }
    
    @IBAction func thirdSelectionAction(_ sender: Any) {
        self.amountField.resignFirstResponder()
        self.thirdSelectionView.backgroundColor = .white
        self.secondSelectionView.backgroundColor = .clear
        self.firstSelectionView.backgroundColor = .clear
        self.amount = "500"
        self.amountField.text = "$500"
    }
    
    @IBAction func requestAction(_ sender: UIButton) {
        if(sender.tag == 1){
            
        }else if(sender.tag == 2){
            
        }
    }
    
    
}

extension AddMoneyViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cardData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionCell", for: indexPath) as! CardCollectionCell
        let val = self.cardData[indexPath.row]
        if(self.selectedCard == val.id){
            cell.cardImage.backgroundColor = K_YELLOW_COLOR
        }else{
            cell.cardImage.backgroundColor = K_OFF_WHITE_COLOR
        }
        cell.cardNumber.text = "\(val.masked_card_number?.dropFirst(8) ?? "")"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCard = self.cardData[indexPath.row].id ?? 0
        self.cardCollection.reloadData()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.thirdSelectionView.backgroundColor = .clear
        self.secondSelectionView.backgroundColor = .clear
        self.firstSelectionView.backgroundColor = .clear
        self.amount = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.amount = self.amountField.text ?? ""
    }
}
