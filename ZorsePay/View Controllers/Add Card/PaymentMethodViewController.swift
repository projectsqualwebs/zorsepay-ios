//
//  PaymentMethodViewController.swift
//  ZorsePay
//
//  Created by Sagar Pandit on 07/07/21.
//  Copyright © 2021 qw. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var transactionTable: ContentSizedTableView!
    @IBOutlet weak var cardCollection: UICollectionView!

    
    var cardData = [GetCardResponse]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

//        NavigationController.shared.getCardData(completionHandler: { (data) in
//            self.cardData = data
//            self.cardCollection.reloadData()
//        })
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension PaymentMethodViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
//            if(self.transactionData.count == 0){
//                self.noTransactionLabel.isHidden = false
//            }else {
//                self.noTransactionLabel.isHidden = true
//            }
            return 20//self.transactionData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableCell") as! TransactionTableCell
//        var val =  self.transactionData[indexPath.row]
//        if(val.receiver_id == Singleton.shared.userDetail.id){
//            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.sender_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
//            cell.userName.text = val.sender_name
//
//        }else {
//            cell.userImage.sd_setImage(with: URL(string:self.getImageURL(text:val.receiver_profile_image ?? "")), placeholderImage: #imageLiteral(resourceName: "round_image"))
//            cell.userName.text = val.receiver_name ?? "You"
//        }
//        if(val.transaction_type == 2){
//            cell.transactionAmount.textColor = redColor
//        }else {
//            cell.transactionAmount.textColor = greenColor
//        }
//        cell.transactionAmount.text = val.amount
//        cell.transactionType.text = val.transaction_method ?? "Stripe"
//        cell.transactionDate.text = "On " + self.convertTimestampToDate(val.created_at ?? 0, to: "MMM dd, yyyy")
        return cell
    }
}


extension PaymentMethodViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20//self.cardData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionCell", for: indexPath) as! CardCollectionCell
//            let val = self.cardData[indexPath.row-1]
//            cell.view1.isHidden = false
//            cell.expiryDate.text = "\(val.expiry_month ?? "")/\(val.expiry_year ?? "")"
//            cell.cardNumber.text = "\(val.masked_card_number?.dropFirst(8) ?? "")"
//            cell.cardholderName.text = val.name_on_card
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width*0.9, height: 180)
    }
}
