//
//  SuccessScreenViewController.swift
//
//  Created by qw on 16/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
import TransitionButton

class SuccessScreenViewController: CustomTransitionViewController {
    
    //MARK:IBOutlets
    @IBOutlet weak var amount: DesignableUILabel!
    @IBOutlet weak var mode: DesignableUILabel!
    @IBOutlet weak var receiverName: DesignableUILabel!
    @IBOutlet weak var receiverNumber: DesignableUILabel!
    @IBOutlet weak var country: DesignableUILabel!
    @IBOutlet weak var serviceStation: DesignableUILabel!
    
    
    var param = [String: Any]()
    var userDetail = SearchUserResponse()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    func initView(){
        self.amount.text = param["amount"] as! String
        if((param["send_from"] as! Int) == 1){
            self.mode.text = "Wallet"
        }else {
            self.mode.text = "Card"
        }
        
        self.receiverName.text = (self.userDetail.first_name ?? "") + " " + (self.userDetail.last_name ?? "")
        self.receiverNumber.text = self.userDetail.phone_number
        self.country.text = self.userDetail.country_name
        self.serviceStation.text = self.userDetail.service_station
    }
    
    //MARK: IBActions
    @IBAction func doneAction(_ sender: Any) {
        Singleton.shared.transactionData = []
        Singleton.shared.walletData = []
        Singleton.shared.contactData = []
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        NavigationController.shared.addTransition(direction:.fromTop, controller: self)
        self.navigationController?.pushViewController(myVC, animated: false)
    }
    
}
