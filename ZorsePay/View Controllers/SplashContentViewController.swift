//
//  SplashContentViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//
import UIKit

class SplashContentViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var currentImage: UIImageView!
    @IBOutlet weak var labelTitle: DesignableUILabel!
    @IBOutlet weak var labelContent: DesignableUILabel!
    @IBOutlet weak var labelSubtitle: DesignableUILabel!
    @IBOutlet weak var viewFour: UIView!
    @IBOutlet weak var viewthree: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewOne: UIView!
    
    
    var content: SplashContent?
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let content = self.content {
            currentImage.image = UIImage(named: content.backgroundImage + ".png")
            labelTitle.text = content.title
            labelContent.text = content.content
        //labelSubtitle.text = content.subTitle
        }
        self.initView(view: index == 0 ? viewOne:index==1 ? viewTwo:index==2 ? viewthree:viewFour)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func initView(view: UIView){
        self.viewOne.backgroundColor = K_OFF_WHITE_COLOR
        self.viewTwo.backgroundColor = K_OFF_WHITE_COLOR
        self.viewthree.backgroundColor = K_OFF_WHITE_COLOR
        self.viewFour.backgroundColor = K_OFF_WHITE_COLOR
        view.backgroundColor = K_YELLOW_COLOR
    }
}
