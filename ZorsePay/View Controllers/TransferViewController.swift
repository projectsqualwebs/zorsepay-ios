//
//  TransferViewController.swift
//
//  Created by qw on 15/08/20.
//  Copyright © 2020 qw. All rights reserved.
//

import UIKit
//import GooglePlaces
import CoreLocation

class TransferViewController: UIViewController,SelectedCard, SelectFromPicker, SelectUser, SelectDate {
    func selectedDate(date: Int) {
        self.selectedDate = date
        self.dob.text = self.convertTimestampToDate(date, to: "dd-MM-yy")
    }
    
    func selectedCardDetail(type: Int, card: GetCardResponse) {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PincodeViewController") as! PincodeViewController
            if(selectedMethod == 1){
                myVC.param = [
                    "amount": self.amount,
                    "user_id": self.selectedUser.id,
                    "send_from": type,
                    "card_id": card.id,
                ]
            }else if(self.selectedMethod == 2){
                myVC.param = [
                    "amount": self.amount,
                    "user_id": self.selectedUser.id,
                    "send_from": type,
                    "card_id": card.id,
                    "postcode": self.selectedPostalCode.id,
                    "service_station": self.selectedServiceStation.id
                ]
            }else if(self.selectedMethod == 3){
                myVC.param = [
                    "amount": self.amount,
                    "user_id": self.selectedUser.id,
                    "payment_gateway": 1,
                    "send_from":type,
                    "card_id": card.id,
                ]
            }
            self.selectedUser.country_name = self.selectedCountry.text
            self.selectedUser.service_station = self.selectedServiceStation.service_station
            myVC.userDetail = self.selectedUser
            myVC.selectedMethod = self.selectedMethod
            if(self.bankDetail.capabilities.card_payments == "active"){
                if(self.accountNumber.text!.isEmpty && self.selectedMethod == 3){
                    Singleton.shared.showToast(text: "Enter account number")
                }else{
                self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else if(selectedMethod == 1 || selectedMethod == 3){
                self.navigationController?.pushViewController(myVC, animated: true)
            }else {
                Singleton.shared.showToast(text: "Bank account is \(self.bankDetail.capabilities.card_payments ?? "")")
            }
        
    }
    
    func getComission(type:Int){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CALCULATE_COMISSION, method: .post, parameter: ["amount": self.amount,"transfer_method":type], objectClass: GetComiission.self, requestCode: U_CALCULATE_COMISSION) { (response) in
            if(type == 2){
                self.codComission.text = "$\(response.response.commission_amount ?? 0)"
            }else if(type == 3){
                self.bankComission.text = "$\(response.response.commission_amount ?? 0)"
            }
        }
    }
    
    func selectedUser(user: SearchUserResponse) {
        self.selectedUser = user
        self.searchField.text = user.first_name
        
        self.firstNameField.text = user.first_name
        self.lastNameField.text = user.last_name
        
        self.phoneNumber.text = (user.phone_number ?? "").applyPatternOnNumbers(pattern: "###-####-#####", replacmentCharacter: "#")
        
         self.getBankDetail(id:user.id ?? 0)
        
    }
    
    func selectedItem(name: String, id: Int) {
        if(currentPicker == 1){
            self.selectedCountry.text = name
            self.getCityData(id: self.countries[id].id ?? 0)
        }else if(currentPicker == 2){
            self.selectedPostalCode = self.postCodeData[id]
            self.getSeriveStation(id: self.selectedPostalCode.id ?? 0)
            self.pincodeField.text = self.selectedPostalCode.postcode
        }else if(currentPicker == 3){
            self.selectedServiceStation = self.ServiceStation[id]
            self.serviceStationField.text = self.selectedServiceStation.service_station
        }else if(self.currentPicker == 4){
            self.selectCity.text = name
            self.getPostalCode(id:self.cities[id].id ?? 0)
        }
    }
    
    //MARK: IBOutelts
    @IBOutlet weak var serviceStationField: DesignableUITextField!
    
    @IBOutlet weak var amountField: DesignableUILabel!
    @IBOutlet weak var lastNameField: DesignableUITextField!
    @IBOutlet weak var pincodeField: DesignableUITextField!
    @IBOutlet weak var firstNameField: DesignableUITextField!
    @IBOutlet weak var searchField: DesignableUITextField!
    @IBOutlet weak var bankView: View!
    @IBOutlet weak var walletView: View!
    @IBOutlet weak var cashView: View!
    @IBOutlet weak var selectedCountry: UITextField!
    @IBOutlet weak var phoneNumber: DesignableUITextField!
    @IBOutlet weak var walletBalance: DesignableUILabel!
    
    @IBOutlet weak var codView: UIStackView!
    @IBOutlet weak var bankStackView: UIStackView!
    @IBOutlet weak var bankDetailView: UIStackView!
    
    @IBOutlet weak var accountNumber: DesignableUITextField!
    @IBOutlet weak var routingNumber: DesignableUITextField!
    @IBOutlet weak var address1: DesignableUITextField!
    @IBOutlet weak var address2: DesignableUITextField!
    @IBOutlet weak var postalCode: DesignableUITextField!
    @IBOutlet weak var city: DesignableUITextField!
    @IBOutlet weak var country: DesignableUITextField!
    @IBOutlet weak var state: DesignableUITextField!
    
    @IBOutlet weak var editButton: CustomButton!
    @IBOutlet weak var codComission: DesignableUILabel!
    @IBOutlet weak var bankComission: DesignableUILabel!
    @IBOutlet weak var selectCity: DesignableUITextField!
    @IBOutlet weak var dob: DesignableUITextField!
    
    
    
    var countries: [Countries] = []
    var cities: [Countries] = []
    
    var amount = String()
    var selectedUser = SearchUserResponse()
    var postCodeData = [Countries]()
    var ServiceStation = [PostCodeResponse]()
    var selectedServiceStation = PostCodeResponse()
    var selectedPostalCode = Countries()
    var currentPicker = Int()
    var sendFrom = Int()
    var selectedMethod = 2
    var bankDetail = BankDetail()
    let locationManager = CLLocationManager()
    var selectedDate = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editButton.setTitle("Edit Detail", for: .normal)
        initView()
        if((self.selectedUser.id ?? 0) != 0){
            self.searchField.text = self.selectedUser.first_name
            self.firstNameField.text = self.selectedUser.first_name
            self.lastNameField.text = self.selectedUser.last_name
            self.phoneNumber.text = (self.selectedUser.phone_number ?? "").applyPatternOnNumbers(pattern: "###-####-#####", replacmentCharacter: "#")
            self.getBankDetail(id:self.selectedUser.id ?? 0)
        }
        NavigationController.shared.getCountryData { (response) in
            self.countries = response
        }
        self.getComission(type: 2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func getBankDetail(id:Int){
        let param: [String:Any] = [
            "type":2,
            "user_id": id,
        ]
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_RETREIVE_BANK_ACCOUNT, method: .post, parameter: param, objectClass: GetBankDetail.self, requestCode: U_RETREIVE_BANK_ACCOUNT) { (response) in
            if(response.response.count > 0){
                self.bankDetail = response.response[0]
               
                self.accountNumber.text = ""
                self.routingNumber.text = self.bankDetail.external_accounts.data[0].routing_number
                self.address1.text = self.bankDetail.company.address.line1
                self.address2.text = self.bankDetail.company.address.line2
                self.postalCode.text = self.bankDetail.company.address.postal_code
                self.city.text = self.bankDetail.company.address.city
                self.country.text = self.bankDetail.company.address.country
                self.state.text = self.bankDetail.company.address.state
                
            }else {
                self.bankDetail = BankDetail()
               
                self.accountNumber.text = ""
                self.routingNumber.text = ""
                self.address1.text = ""
                self.address2.text = ""
                self.postalCode.text = ""
                self.city.text = ""
                self.country.text = ""
                self.state.text = ""
                Singleton.shared.showToast(text: "No Bank Detail Found")
            }
        }
    }
    
    func getPostalCode(id: Int){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_POSTCODE_CITY + "\(id)", method: .get, parameter: nil, objectClass:  GetCities.self, requestCode: U_GET_POSTCODE_CITY) { (response) in
            self.postCodeData = response.response.cities
            ActivityIndicator.hide()
        }
    }
    
    func getCityData(id: Int){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CITIES + "\(id)", method: .get, parameter: nil, objectClass: GetCities.self, requestCode: U_GET_CITIES) { (response) in
            self.cities = response.response.cities
            ActivityIndicator.hide()
        }
    }
    
    func getSeriveStation(id: Int){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SERVICE_STATION + "\(id)", method: .get, parameter: nil, objectClass: GetPostCode.self, requestCode: U_GET_SERVICE_STATION) { (response) in
            self.ServiceStation = response.response
            if(response.response.count > 0){
                self.serviceStationField.text = response.response[0].service_station
                self.selectedServiceStation = response.response[0]
            }
            ActivityIndicator.hide()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func initView(){
        self.accountNumber.delegate = self
        self.postalCode.delegate = self
        self.routingNumber.delegate = self
        self.amountField.text = "$" + self.amount
        if(Singleton.shared.walletData.count > 0){
            self.walletBalance.text = "$" + (Singleton.shared.walletData[0].balance ?? "")
        }
    }
    
    //MARK: IBActions
    @IBAction func editAction(_ sender: Any) {
        if(self.editButton.titleLabel?.text == "Update Detail"){
            if(self.selectedUser.id == nil){
                Singleton.shared.showToast(text: "Select user")
            }else if(self.accountNumber.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Account Number")
            }else if(self.routingNumber.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Routing Number")
            }else if(self.address1.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Address 1")
            }else if(self.address2.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Address 2")
            }else if(self.postalCode.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Postal Code")
            }else if(self.city.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter City")
            }else if(self.state.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter State")
            }else if(self.country.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Country")
            }else if(self.dob.text!.isEmpty){
                Singleton.shared.showToast(text: "Select Date Of Birth")
            }else {
                ActivityIndicator.show(view: self.view)
                let param:[String:Any] = [
                    "account_number" : self.accountNumber.text ?? "",
                    "routing_number" : self.routingNumber.text ?? "",
                    "city" : self.city.text ?? "",
                    "country" : self.country.text ?? "",
                    "line1" : self.address1.text ?? "",
                    "line2": self.address2.text ?? "",
                    "postal_code": self.postalCode.text ?? "",
                    "state" : self.state.text ?? "",
                    "date_of_birth" : self.selectedDate,
                    "type":2,
                    "user_id":self.selectedUser.id
                ]
                var url = String()
                if(self.bankDetail.company.address.city == nil){
                    url = U_BASE + U_REGISTER_BANK_ACCOUNT
                }else {
                    url =  U_BASE + U_UPDATE_BANK_ACCOUNT
                }
                SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_BANK_ACCOUNT) { (response) in
                    self.editButton.setTitle("Edit Detail", for: .normal)
                    self.bankDetailView.isUserInteractionEnabled = false
                    ActivityIndicator.hide()
                }
            }
        }else {
            self.editButton.setTitle("Update Detail", for: .normal)
            self.bankDetailView.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.dateDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func countryPickerAction(_ sender: Any) {
        self.currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.countries{
            myVC.pickerData.append(val.country ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerDelegate = self
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectCityAction(_ sender: Any) {
        self.currentPicker = 4
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.cities{
            myVC.pickerData.append(val.city ?? "")
        }
        
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerDelegate = self
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func addressButtonAction(_ sender: Any) {
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        
//        // Specify the place data types to return.
//        let fields: GMSPlaceField = GMSPlaceField(rawValue:UInt(GMSPlaceField.name.rawValue) |
//                                                    UInt(GMSPlaceField.placeID.rawValue) |
//                                                    UInt(GMSPlaceField.coordinate.rawValue) |
//                                                    GMSPlaceField.addressComponents.rawValue |
//                                                    GMSPlaceField.formattedAddress.rawValue)
//        // autocompleteController.placeFields = .formattedAddress
//        
//        // Specify a filter.
//        let filter = GMSAutocompleteFilter()
//        filter.type = .address
//        
//        autocompleteController.autocompleteFilter = filter
//        
//        // Display the autocomplete view controller.
//        autocompleteController.modalPresentationStyle = .overFullScreen
//        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if(self.selectedUser.id == nil){
            Singleton.shared.showToast(text: "Please select user to transfer amount")
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectCardViewController") as! SelectCardViewController
            myVC.modalPresentationStyle = .overFullScreen
            myVC.cardDelegate = self
            if(self.selectedMethod == 1){
                myVC.isWalletSelected = true
            }
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func walletAction(_ sender: Any) {
        selectedMethod = 1
        self.codView.isHidden = true
        self.bankStackView.isHidden = true
        self.walletView.borderColor = .black
        self.cashView.borderColor = .clear
        self.bankView.borderColor = .clear
    }
    
    @IBAction func cashAction(_ sender: Any) {
        selectedMethod = 2
        if(self.codComission.text!.isEmpty){
            self.getComission(type: 2)
        }
        self.codView.isHidden = false
        self.bankStackView.isHidden = true
        self.walletView.borderColor = .clear
        self.cashView.borderColor = .black
        self.bankView.borderColor = .clear
    }
    
    @IBAction func bankAction(_ sender: Any) {
        selectedMethod = 3
        if(self.bankComission.text!.isEmpty){
            self.getComission(type: 3)
        }
        self.codView.isHidden = true
        self.bankStackView.isHidden = false
        self.walletView.borderColor = .clear
        self.cashView.borderColor = .clear
        self.bankView.borderColor = .black
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchUserViewController") as! SearchUserViewController
        myVC.searchDelegate = self
        self.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func pincodeAction(_ sender: Any) {
        self.currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.postCodeData{
            myVC.pickerData.append(val.postcode ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerDelegate = self
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func serviceStationAction(_ sender: Any) {
        self.currentPicker = 3
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.ServiceStation{
            myVC.pickerData.append(val.service_station ?? "")
        }
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerDelegate = self
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
        
    }
}


extension TransferViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == accountNumber){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 16) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == pincodeField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 6) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == routingNumber){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 10) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == postalCode){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count < 6) {
                    return true
                }else {
                    return false
                }
            }
        }else if(textField == phoneNumber){
            let currentCharacterCount = self.phoneNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            self.phoneNumber.text = self.phoneNumber.text?.applyPatternOnNumbers(pattern: "###-####-#####", replacmentCharacter: "#")
            return newLength <= 14
        }
        
        return true
    }
    
    func latLong(lat: Double,long: Double)  {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat , longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            print("Response GeoLocation : \(placemarks)")
            var placeMark: CLPlacemark!
            if let placeMark = placemarks?[0]{
                
                // Country
                if let country = placeMark.addressDictionary?["CountryCode"] as? String {
                    self.country.text = country
                    if let city = placeMark.addressDictionary?["City"] as? String {
                        self.city.text = city
                        
                        // State
                        if let state = placeMark.addressDictionary?["State"] as? String{
                            self.state.text = state
                            
                            if let street = placeMark.addressDictionary?["Street"] as? String{
                                print("Street :- \(street)")
                                let str = street
                                let streetNumber = str.components(
                                    separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                                print("streetNumber :- \(streetNumber)" as Any)
                                
                                // ZIP
                                if let zip = placeMark.addressDictionary?["ZIP"] as? String{
                                    self.postalCode.text = zip
                                    // Location name
                                    if let locationName = placeMark.addressDictionary?["Name"] as? String {
                                        print("Location Name :- \(locationName)")
                                        // Street address
                                        
                                    }
                                }
                            }else {
                                if(self.address1.text!.isEmpty){
                                    self.address1.text =  "\(city)" + ", \(state)"
                                }
                            }
                        }
                    }
                }
            }
        })
    }
}

//extension TransferViewController: GMSAutocompleteViewControllerDelegate, CLLocationManagerDelegate {
//    // Handle the user's selection.
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        self.address1.text = place.formattedAddress
//        let latitude = place.coordinate.latitude
//        let longitude = place.coordinate.longitude
//        self.latLong(lat: Double(latitude), long:Double(longitude))
//        dismiss(animated: true, completion: nil)
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        print("Error: ", error.localizedDescription)
//    }
//
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    // Turn the network activity indicator on and off again.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//
//}
